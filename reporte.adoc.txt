= REPORTE
// declarar la variables para le documento
:notitle:
:lang: es
:imagesdir: ./images
include::attributes-es.adoc[]
// se puede incluir un logo de cabecera

image::logo.png[align=right]

[.text-center]
*REPORTE*

[cols="30%,10%,~", frame=none, grid=none]
|===
|      |Para:   |Tu Unidad
|      |De:     |*Nombre Completo*
|      |        |_Cargo_
|      |Asunto: |Tema
|      |Fecha:  |[día] [mes] [año]
|===

'''

== Objetivos
Detallar los objetivos a cumplir en la actividad.

=== Objetivos Específicos:

[cols=".^2a,3", frame=none, grid=none]
|===
|. Detallar las tareas especificas a cumplir.
. Logros que se quieren obtener.
|image:mapa.jpg[width=300%, align=center]
// se puede listar varios objetivos
// tambien se puede incluir un mapa de localización
|===

// se puede incluir el cronograma de trabajo
[caption="Cronograma: "]  
.Plan de trabajo
[cols="1*^,2", frame=ends, grid=rows]
|===
|Día
<|Detalle

|Día 1 [día][mes][año]
<|Actividad realizada

|Día 2 [día][mes][año]
<|Actividad realizada
|===

// para el salto de página utilizar <<<
// para dejar un renglón vacío utilizar {empty} +
<<<
{empty} +    

== Sección 1
=== Sección 1-1
// se puede dar formato al texto
* Texto aquí, *texto aquí*, _texto aquí_, [.underline]#texto aquí#, #texto aquí#...
// también se puede incluir fotos y texto breve

[cols="2*^", frame=sides, grid=cols]
|===
|image:foto.jpg[width=150%, align=center]
<.^|Texto aquí.

|image:foto.jpg[width=150%, align=center]
<.^|Más texto aquí.
|===

=== Sección2

* Texto aquí...
// mas imágenes para referencia, se puede ajustar el zoom

image::foto.jpg[width=25%, align=center]

image::foto.jpg[width=50%, align=center]

<<<
{empty} +    

== RESULTADOS
=== Conclusiones

. Texto aquí.
. Texto aquí.

// se pueden agregar notas
[NOTE]
.Notas adicionales
Texto aquí.

=== Recomendaciones

== OBSERVACIONES
=== Otros
:figure-caption!:

{empty} + 

*Firmas*

.RESPONSABLE 1: *NOMBRE COMPLETO*
image::firma.jpg[width=30%, align=left]
